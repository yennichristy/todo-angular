import { NgModule } from '@angular/core';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

const material = [
  MatGridListModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  ReactiveFormsModule,
];

@NgModule({
  imports: [material],
  exports: [material],
})
export class MaterialModule {}
