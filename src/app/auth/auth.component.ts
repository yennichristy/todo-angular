import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  @ViewChild('getValue') loginForm: NgForm;

  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(private _auth: AuthService) {}

  onSubmit() {
    this.loginForm.value.email;
    this.loginForm.value.password;
    console.log(this.loginForm);
  }
}
