import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _loginUrl = 'https://miniprojectc.herokuapp.com/api/v1/users/login';

  constructor(private http: HttpClient) {}

  loginUser(email: string, password: string) {
    return this.http
      .post(this._loginUrl, { email, password })
      .subscribe((resData) => {
        console.log('res', resData);
      });
  }
}
